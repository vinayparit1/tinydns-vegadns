#!/bin/sh

IP=`hostname -i`

if [ ! -d /etc/tinydns ];
then
	cd /package/admin/daemontools-0.76 ; package/install
	cd /package/ucspi-tcp-0.88 ; make ; make setup check
	cd /package/djbdns-1.05 ; make ; make setup check
	tinydns-conf tinydns dnslog /etc/tinydns $IP
	ln -s /etc/tinydns /service
	ln -s /etc/sv/apache2 /service
	ln -s /etc/sv/mysql   /service
else

sh /var/www/html/VegaDNS/update-data.sh
exec /command/svscanboot

fi
