FROM debian:stretch
MAINTAINER VINAYAK <paritvinay@gmail.com>
RUN apt-get update
RUN apt-get -y install wget vim curl build-essential systemd apache2 php php-mysql mysql-server
RUN mkdir /package
RUN mkdir -p /etc/sv/apache2
RUN chmod 1755 /package
RUN bash -c 'mkdir /package/{admin,ucspi-tcp-0.88,djbdns-1.05}'
RUN mkdir /etc/sv/mysql
RUN mkdir /var/www/html/VegaDNS
ADD VegaDNS /var/www/html/VegaDNS
ADD mysql /etc/sv/mysql
ADD admin /package/admin
ADD ucspi-tcp-0.88 /package/ucspi-tcp-0.88
ADD djbdns-1.05 /package/djbdns-1.05
ADD apache2 /etc/sv/apache2/
COPY vegadns.sql /opt/
RUN /usr/sbin/useradd -s /bin/false tinydns
RUN /usr/sbin/useradd -s /bin/false dnslog
RUN bash -c 'mkdir /var/www/html/VegaDNS/{sessions,templates_c,configs,cache}'
RUN bash -c 'chown www-data:www-data /var/www/html/VegaDNS/{sessions,templates,templates_c,configs,cache}'
ADD entrypoint.sh /
CMD ["./entrypoint.sh"]
